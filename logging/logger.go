package logging

import (
	"flag"
	stackdriver "github.com/TV4/logrus-stackdriver-formatter"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/grpclog"
	"os"
	"time"
)

var Default *logrus.Logger
var json = flag.Bool("json-log", false, "Log to json")
var debugGrpc = flag.Bool("debug-grpc", false, "Debug GRPC calls")
var traceMode = flag.Bool("trace", false, "Verbose logging")

func InitLogger() {
	Default = logrus.StandardLogger()
	logrus.SetLevel(logrus.DebugLevel)
	if *traceMode {
		logrus.SetLevel(logrus.TraceLevel)
	}
	logrus.SetOutput(os.Stdout)
	if *json {
		logrus.SetFormatter(stackdriver.NewFormatter(
		//stackdriver.WithService(os.Args[0]),
		//stackdriver.WithVersion("v0.1.0"),
		))
	} else {
		logrus.SetFormatter(&logrus.TextFormatter{
			ForceColors:     true,
			FullTimestamp:   true,
			TimestampFormat: time.RFC3339Nano,
			DisableSorting:  true,
		})
	}
	// Should only be done from init functions
	if *debugGrpc {
		grpclog.SetLoggerV2(grpclog.NewLoggerV2(Default.Out, Default.Out, Default.Out))
	}
}
