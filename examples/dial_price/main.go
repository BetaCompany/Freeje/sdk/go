package main

import (
	_ "embed"
	"flag"
	freeje_client "gitlab.com/BetaCompany/Freeje/sdk/go/api/client"
	"gitlab.com/BetaCompany/Freeje/sdk/go/helpers"
	"gitlab.com/BetaCompany/Freeje/sdk/go/logging"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

var endpoint = flag.String("endpoint", "api.freeje.org:443", "Default api endpoint")
var insecure = flag.Bool("insecure", false, "Skip TLS verification")
var dial = flag.String("dial", "+79990000000", "Numbers to estimate")

func main() {
	flag.Parse()
	logging.InitLogger()
	conn, err := helpers.NewApiConnection(*endpoint, *insecure)
	if err != nil {
		logging.Default.Fatalf("did not connect: %s", err)
	}
	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			logging.Default.Errorf("did not connect: %s", err)
		}
	}(conn)

	callClient := freeje_client.NewCallClient(conn)
	if estimate, err := callClient.Estimate(context.Background(), &freeje_client.CallNumber{Dial: *dial}); err != nil {
		logging.Default.Error("Unable to get call estimate %v:", err)
	} else {
		logging.Default.Infof("The call to %v estimated to %+v", *dial, estimate)
	}
}
