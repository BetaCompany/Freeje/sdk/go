package main

import (
	_ "embed"
	"flag"
	freeje_api "gitlab.com/BetaCompany/Freeje/sdk/go/api"
	freeje_client "gitlab.com/BetaCompany/Freeje/sdk/go/api/client"
	"gitlab.com/BetaCompany/Freeje/sdk/go/helpers"
	"gitlab.com/BetaCompany/Freeje/sdk/go/logging"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"io"
)

var endpoint = flag.String("endpoint", "api.freeje.org:443", "Default api endpoint")
var insecure = flag.Bool("insecure", false, "Skip TLS verification")
var searchPrefix = flag.String("search", "176", "Numbers with specified prefix to search for")

func main() {
	flag.Parse()
	logging.InitLogger()
	conn, err := helpers.NewApiConnection(*endpoint, *insecure)
	if err != nil {
		logging.Default.Fatalf("did not connect: %s", err)
	}
	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			logging.Default.Errorf("did not connect: %s", err)
		}
	}(conn)

	didClient := freeje_client.NewDIDClient(conn)
	localities, err := didClient.ListAvailableLocalities(context.Background(), &freeje_api.RequestMode{Streamed: false})
	if err != nil {
		logging.Default.Fatal(err)
	}
	for {
		locality, err := localities.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			logging.Default.Fatalf("ListAvailableLocalities(_) = _, %v", err)
		}
		logging.Default.Infof("Locality: %v", locality)
	}
	logging.Default.Println()
	logging.Default.Infof("Numbers with prefix %s:", *searchPrefix)
	if numbers, err := didClient.ListAvailableNumbers(context.Background(), &freeje_client.DidLocality{Prefix: *searchPrefix}); err == nil {
		for {
			if locality, err := numbers.Recv(); err != nil {
				if err == io.EOF {
					break
				}
				logging.Default.Fatalf("ListAvailableNumbers(_) = _, %v", err)

			} else {
				logging.Default.Infof("%v", locality)
			}
		}
	} else {
		logging.Default.Error("Unable to receive numbers %v:", err)
	}
}
