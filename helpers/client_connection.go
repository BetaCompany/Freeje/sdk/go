package helpers

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"gitlab.com/BetaCompany/Freeje/sdk/go/localauth"
	"gitlab.com/BetaCompany/Freeje/sdk/go/logging"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"strings"
)

type FreejeTokenAuth struct {
	token string
}

func (auth FreejeTokenAuth) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{"token": auth.token}, nil
}

func (FreejeTokenAuth) RequireTransportSecurity() bool {
	return true
}

func NewTokenApiConnection(apiEndpoint string, insecure bool, token string) (*grpc.ClientConn, error) {
	if !strings.Contains(apiEndpoint, ":") {
		apiEndpoint += ":443"
	}
	pool, _ := x509.SystemCertPool()
	transportCreds := credentials.NewTLS(&tls.Config{
		InsecureSkipVerify: insecure,
		RootCAs:            pool,
	})
	return grpc.Dial(
		apiEndpoint,
		grpc.WithTransportCredentials(transportCreds),
		grpc.WithPerRPCCredentials(&FreejeTokenAuth{token: token}))

}
func NewApiConnection(apiEndpoint string, insecure bool) (*grpc.ClientConn, error) {
	if !strings.Contains(apiEndpoint, ":") {
		apiEndpoint += ":443"
	}
	pool, _ := x509.SystemCertPool()
	creds, err := localauth.GetLocalTokenSource(context.Background())
	if err != nil {
		logging.Default.Fatal(err)
	}
	transportCreds := credentials.NewTLS(&tls.Config{
		InsecureSkipVerify: insecure,
		RootCAs:            pool,
	})

	return grpc.Dial(
		apiEndpoint,
		grpc.WithTransportCredentials(transportCreds),
		grpc.WithPerRPCCredentials(&localauth.FreejePerRpcCredentials{Token: creds}))

}
