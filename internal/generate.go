//go:generate rm -rf ../api
//go:generate mkdir -p ../api
//go:generate sh -c "cd ../../proto && git pull"
//go:generate sh -c "protoc -I ../../proto/com/freeje/api --go-grpc_opt=require_unimplemented_servers=false,module=gitlab.com/BetaCompany/Freeje/sdk/go --go-grpc_out=..  --go_opt=module=gitlab.com/BetaCompany/Freeje/sdk/go --go_out=.. $(find -L ../../proto/com/freeje/api -type f -name '*.proto' -not -path '*/analytics/*' -printf '%P\n')"
package grpc
