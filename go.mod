module gitlab.com/BetaCompany/Freeje/sdk/go

go 1.17

//go get -u ./...
//go build ./...

require (
	cloud.google.com/go v0.97.0 // indirect
	github.com/TV4/logrus-stackdriver-formatter v0.1.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/shibukawa/configdir v0.0.0-20170330084843-e180dbdc8da0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/net v0.0.0-20210716203947-853a461950ff
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
	golang.org/x/sys v0.0.0-20210908233432-aa78b53d3365 // indirect
	google.golang.org/genproto v0.0.0-20211112145013-271947fe86fd // indirect
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/appengine v1.6.7 // indirect
)
