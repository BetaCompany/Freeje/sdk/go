package localauth

import (
	"context"
	"embed"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/shibukawa/configdir"
	"gitlab.com/BetaCompany/Freeje/sdk/go/logging"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"
)

func openbrowser(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		logging.Default.Error(err)
	}

}

type GoogleRefreshTokenSource struct {
	token *oauth2.Token
}

const freejeWebApiKey = "AIzaSyAsCd6AiVKyZLfTP0CwFk8hmO6N3E_CNWA"

func GetCustomSignInToken(token string) (oauth2.TokenSource, error) {
	rq, _ := json.Marshal(map[string]interface{}{
		"token":             token,
		"returnSecureToken": true,
	})
	resp, err := http.Post(
		"https://identitytoolkit.googleapis.com/v1/accounts:signInWithCustomToken?key="+freejeWebApiKey,
		"application/json",
		strings.NewReader(string(rq)),
	)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = resp.Body.Close()
	}()
	var values = map[string]interface{}{}
	if err := json.NewDecoder(resp.Body).Decode(&values); err != nil {
		return nil, err
	}
	ex, _ := strconv.Atoi(fmt.Sprintf("%s", values["expiresIn"]))
	if ex == 0 {
		ex = 3600
	}
	var newToken = &GoogleRefreshTokenSource{token: &oauth2.Token{
		AccessToken:  fmt.Sprintf("%v", values["idToken"]),
		RefreshToken: fmt.Sprintf("%v", values["refreshToken"]),
		Expiry:       time.Now().Add(time.Duration(ex) * time.Second),
	}}
	return newToken, nil
}
func (rt *GoogleRefreshTokenSource) Token() (*oauth2.Token, error) {
	if !rt.token.Valid() {
		resp, err := http.PostForm(
			"https://securetoken.googleapis.com/v1/token?key="+freejeWebApiKey,
			url.Values{
				"grant_type":    {"refresh_token"},
				"refresh_token": {rt.token.RefreshToken},
			},
		)
		if err != nil {
			return nil, err
		}
		var values = map[string]string{}
		defer func() {
			_ = resp.Body.Close()
		}()
		if err := json.NewDecoder(resp.Body).Decode(&values); err != nil {
			return nil, err
		}
		ex, _ := strconv.Atoi(values["expires_in"])
		if ex == 0 {
			ex = 3600
		}
		var newToken = &oauth2.Token{
			AccessToken:  values["access_token"],
			RefreshToken: values["refresh_token"],
			TokenType:    values["token_type"],
			Expiry:       time.Now().Add(time.Duration(ex) * time.Second),
		}
		_ = folder.WriteFile(accessTokenFile, []byte(newToken.AccessToken))
		_ = folder.WriteFile(retreshTokenFile, []byte(newToken.RefreshToken))
		rt.token = newToken
		//return newToken, nil

	}
	//logging.Default.Infof("Current access token %v", rt.token.AccessToken)
	return rt.token, nil
}

type FreejePerRpcCredentials struct {
	Token  oauth2.TokenSource
	Secure bool
}

func (s *FreejePerRpcCredentials) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	t, e := s.Token.Token()
	if e != nil {
		return nil, e
	}
	return map[string]string{
		"JWT": t.AccessToken,
	}, nil
}

func (s *FreejePerRpcCredentials) RequireTransportSecurity() bool {
	return s.Secure
}

func GetLocalCreds(ctx context.Context) (*google.Credentials, error) {
	source, err := GetLocalTokenSource(ctx)
	if err != nil {
		return nil, err
	}
	return &google.Credentials{TokenSource: source}, nil
}

var folder = configdir.New("freeje", "authtoken").QueryCacheFolder()

const retreshTokenFile = "refresh.token"
const accessTokenFile = "access.token"
const idTokenFile = "id.token"

func GetLocalIdToken(ctx context.Context) (*oauth2.Token, error) {
	idToken, _ := folder.ReadFile(idTokenFile)
	stdClaims := &jwt.StandardClaims{}
	_, _ = jwt.ParseWithClaims(string(idToken), stdClaims, func(token *jwt.Token) (interface{}, error) {
		return []byte(""), nil
	})
	if !stdClaims.VerifyExpiresAt(time.Now().Unix(), true) {
		tokens, err := SignInUser(ctx)
		if err != nil {
			return nil, err
		}
		return tokens, nil
	}
	return &oauth2.Token{
		AccessToken: string(idToken),
		Expiry:      time.Unix(stdClaims.ExpiresAt, 0),
	}, nil
}

func GetLocalTokenSource(ctx context.Context) (oauth2.TokenSource, error) {
	var creds oauth2.TokenSource
	retreshToken, _ := folder.ReadFile(retreshTokenFile)
	accessToken, _ := folder.ReadFile(accessTokenFile)
	stdClaims := &jwt.StandardClaims{}
	_, _ = jwt.ParseWithClaims(string(accessToken), stdClaims, func(token *jwt.Token) (interface{}, error) {
		return []byte(""), nil
	})
	if stdClaims.ExpiresAt == 0 {
		accessToken = nil
	}
	creds = &GoogleRefreshTokenSource{
		token: &oauth2.Token{
			AccessToken:  string(accessToken),
			RefreshToken: string(retreshToken),
			Expiry:       time.Unix(stdClaims.ExpiresAt, 0),
		}}
	if _, err := creds.Token(); err == nil {
		return creds, nil
	}
	tokens, err := SignInUser(ctx)
	if err != nil {
		return nil, err
	}
	return &GoogleRefreshTokenSource{
		token: &oauth2.Token{
			RefreshToken: tokens.RefreshToken,
		}}, nil

}

//go:embed jwtauth.html loader.svg
var content embed.FS

func SignInUser(ctx context.Context) (*oauth2.Token, error) {
	tokenChan := make(chan *oauth2.Token)
	errChan := make(chan error)
	authFs := http.FileServer(http.FS(content))
	httpSrv := &http.Server{
		Handler: http.HandlerFunc(func(response http.ResponseWriter, rq *http.Request) {
			if strings.HasPrefix(rq.URL.Path, "/token/") {
				http.Error(response, "OK", http.StatusOK)
				parts := strings.Split(rq.URL.Path, "/")
				tokens := &oauth2.Token{}

				if len(parts) > 2 {
					tokens.RefreshToken = parts[2]
				}
				if len(parts) > 3 {
					tokens.AccessToken = parts[3]
				}
				_ = folder.WriteFile(idTokenFile, []byte(tokens.AccessToken))
				_ = folder.WriteFile(retreshTokenFile, []byte(tokens.RefreshToken))
				tokenChan <- tokens
				return
			}
			if rq.URL.Path == "/" {
				rq.URL.Path = "/jwtauth.html"
			}
			authFs.ServeHTTP(response, rq)
		}),
		ReadHeaderTimeout: 5 * time.Second,
		IdleTimeout:       120 * time.Second,
	}
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		return nil, err
	}
	port := listener.Addr().(*net.TCPAddr).Port
	url := fmt.Sprintf("http://localhost:%v", port)
	_, _ = fmt.Fprintf(os.Stderr, "Using url to authenticate %s\n", url)
	go func() {
		defer close(errChan)
		defer close(tokenChan)
		errChan <- httpSrv.Serve(listener)
	}()
	defer func() {
		_ = httpSrv.Shutdown(ctx)
	}()
	openbrowser(url)
	select {
	case <-time.After(5 * time.Minute):
		return nil, errors.New("timeout")
	case <-ctx.Done():
		return nil, ctx.Err()
	case err := <-errChan:
		return nil, err
	case tokenPair := <-tokenChan:
		return tokenPair, err
	}
}
