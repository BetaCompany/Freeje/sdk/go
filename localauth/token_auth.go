package localauth

import "context"

type FreejeTokenAuth struct {
	Token string
}

func (auth FreejeTokenAuth) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{"token": auth.Token}, nil
}

func (FreejeTokenAuth) RequireTransportSecurity() bool {
	return true
}
