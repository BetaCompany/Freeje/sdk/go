// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.18.1
// source: client/payment.proto

package freeje_client

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Method struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Description string  `protobuf:"bytes,2,opt,name=description,proto3" json:"description,omitempty"`
	Request     string  `protobuf:"bytes,3,opt,name=request,proto3" json:"request,omitempty"`
	Amount      float64 `protobuf:"fixed64,4,opt,name=amount,proto3" json:"amount,omitempty"`
	FinalAmount float64 `protobuf:"fixed64,5,opt,name=final_amount,json=finalAmount,proto3" json:"final_amount,omitempty"`
	Currency    string  `protobuf:"bytes,6,opt,name=currency,proto3" json:"currency,omitempty"`
	Type        string  `protobuf:"bytes,7,opt,name=type,proto3" json:"type,omitempty"`
	Rate        float64 `protobuf:"fixed64,8,opt,name=rate,proto3" json:"rate,omitempty"`
	Geo         string  `protobuf:"bytes,9,opt,name=geo,proto3" json:"geo,omitempty"`
}

func (x *Method) Reset() {
	*x = Method{}
	if protoimpl.UnsafeEnabled {
		mi := &file_client_payment_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Method) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Method) ProtoMessage() {}

func (x *Method) ProtoReflect() protoreflect.Message {
	mi := &file_client_payment_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Method.ProtoReflect.Descriptor instead.
func (*Method) Descriptor() ([]byte, []int) {
	return file_client_payment_proto_rawDescGZIP(), []int{0}
}

func (x *Method) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Method) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *Method) GetRequest() string {
	if x != nil {
		return x.Request
	}
	return ""
}

func (x *Method) GetAmount() float64 {
	if x != nil {
		return x.Amount
	}
	return 0
}

func (x *Method) GetFinalAmount() float64 {
	if x != nil {
		return x.FinalAmount
	}
	return 0
}

func (x *Method) GetCurrency() string {
	if x != nil {
		return x.Currency
	}
	return ""
}

func (x *Method) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

func (x *Method) GetRate() float64 {
	if x != nil {
		return x.Rate
	}
	return 0
}

func (x *Method) GetGeo() string {
	if x != nil {
		return x.Geo
	}
	return ""
}

type Invoice struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id           string            `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	PayId        string            `protobuf:"bytes,2,opt,name=pay_id,json=payId,proto3" json:"pay_id,omitempty"`
	Request      string            `protobuf:"bytes,3,opt,name=request,proto3" json:"request,omitempty"`
	Product      string            `protobuf:"bytes,4,opt,name=product,proto3" json:"product,omitempty"`
	Amount       float64           `protobuf:"fixed64,5,opt,name=amount,proto3" json:"amount,omitempty"`
	FinalAmount  float64           `protobuf:"fixed64,6,opt,name=final_amount,json=finalAmount,proto3" json:"final_amount,omitempty"`
	Currency     string            `protobuf:"bytes,7,opt,name=currency,proto3" json:"currency,omitempty"`
	ActionUrl    string            `protobuf:"bytes,8,opt,name=action_url,json=actionUrl,proto3" json:"action_url,omitempty"`
	ActionMethod string            `protobuf:"bytes,9,opt,name=action_method,json=actionMethod,proto3" json:"action_method,omitempty"`
	Parameters   map[string]string `protobuf:"bytes,10,rep,name=parameters,proto3" json:"parameters,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	Rate         float64           `protobuf:"fixed64,11,opt,name=rate,proto3" json:"rate,omitempty"`
	SuccessUrl   string            `protobuf:"bytes,12,opt,name=success_url,json=successUrl,proto3" json:"success_url,omitempty"`
	FailUrl      string            `protobuf:"bytes,13,opt,name=fail_url,json=failUrl,proto3" json:"fail_url,omitempty"`
	Name         string            `protobuf:"bytes,14,opt,name=name,proto3" json:"name,omitempty"`
	Url          string            `protobuf:"bytes,15,opt,name=url,proto3" json:"url,omitempty"`
}

func (x *Invoice) Reset() {
	*x = Invoice{}
	if protoimpl.UnsafeEnabled {
		mi := &file_client_payment_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Invoice) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Invoice) ProtoMessage() {}

func (x *Invoice) ProtoReflect() protoreflect.Message {
	mi := &file_client_payment_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Invoice.ProtoReflect.Descriptor instead.
func (*Invoice) Descriptor() ([]byte, []int) {
	return file_client_payment_proto_rawDescGZIP(), []int{1}
}

func (x *Invoice) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Invoice) GetPayId() string {
	if x != nil {
		return x.PayId
	}
	return ""
}

func (x *Invoice) GetRequest() string {
	if x != nil {
		return x.Request
	}
	return ""
}

func (x *Invoice) GetProduct() string {
	if x != nil {
		return x.Product
	}
	return ""
}

func (x *Invoice) GetAmount() float64 {
	if x != nil {
		return x.Amount
	}
	return 0
}

func (x *Invoice) GetFinalAmount() float64 {
	if x != nil {
		return x.FinalAmount
	}
	return 0
}

func (x *Invoice) GetCurrency() string {
	if x != nil {
		return x.Currency
	}
	return ""
}

func (x *Invoice) GetActionUrl() string {
	if x != nil {
		return x.ActionUrl
	}
	return ""
}

func (x *Invoice) GetActionMethod() string {
	if x != nil {
		return x.ActionMethod
	}
	return ""
}

func (x *Invoice) GetParameters() map[string]string {
	if x != nil {
		return x.Parameters
	}
	return nil
}

func (x *Invoice) GetRate() float64 {
	if x != nil {
		return x.Rate
	}
	return 0
}

func (x *Invoice) GetSuccessUrl() string {
	if x != nil {
		return x.SuccessUrl
	}
	return ""
}

func (x *Invoice) GetFailUrl() string {
	if x != nil {
		return x.FailUrl
	}
	return ""
}

func (x *Invoice) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *Invoice) GetUrl() string {
	if x != nil {
		return x.Url
	}
	return ""
}

type StripeData struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Currency string  `protobuf:"bytes,1,opt,name=currency,proto3" json:"currency,omitempty"`
	Amount   float64 `protobuf:"fixed64,2,opt,name=amount,proto3" json:"amount,omitempty"`
	Token    string  `protobuf:"bytes,3,opt,name=token,proto3" json:"token,omitempty"`
}

func (x *StripeData) Reset() {
	*x = StripeData{}
	if protoimpl.UnsafeEnabled {
		mi := &file_client_payment_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StripeData) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StripeData) ProtoMessage() {}

func (x *StripeData) ProtoReflect() protoreflect.Message {
	mi := &file_client_payment_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StripeData.ProtoReflect.Descriptor instead.
func (*StripeData) Descriptor() ([]byte, []int) {
	return file_client_payment_proto_rawDescGZIP(), []int{2}
}

func (x *StripeData) GetCurrency() string {
	if x != nil {
		return x.Currency
	}
	return ""
}

func (x *StripeData) GetAmount() float64 {
	if x != nil {
		return x.Amount
	}
	return 0
}

func (x *StripeData) GetToken() string {
	if x != nil {
		return x.Token
	}
	return ""
}

type StripeResult struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Success bool   `protobuf:"varint,1,opt,name=success,proto3" json:"success,omitempty"`
	Code    string `protobuf:"bytes,2,opt,name=code,proto3" json:"code,omitempty"`
	Message string `protobuf:"bytes,3,opt,name=message,proto3" json:"message,omitempty"`
}

func (x *StripeResult) Reset() {
	*x = StripeResult{}
	if protoimpl.UnsafeEnabled {
		mi := &file_client_payment_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StripeResult) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StripeResult) ProtoMessage() {}

func (x *StripeResult) ProtoReflect() protoreflect.Message {
	mi := &file_client_payment_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StripeResult.ProtoReflect.Descriptor instead.
func (*StripeResult) Descriptor() ([]byte, []int) {
	return file_client_payment_proto_rawDescGZIP(), []int{3}
}

func (x *StripeResult) GetSuccess() bool {
	if x != nil {
		return x.Success
	}
	return false
}

func (x *StripeResult) GetCode() string {
	if x != nil {
		return x.Code
	}
	return ""
}

func (x *StripeResult) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

type ApplePayData struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	JsonData string `protobuf:"bytes,1,opt,name=json_data,json=jsonData,proto3" json:"json_data,omitempty"`
}

func (x *ApplePayData) Reset() {
	*x = ApplePayData{}
	if protoimpl.UnsafeEnabled {
		mi := &file_client_payment_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ApplePayData) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ApplePayData) ProtoMessage() {}

func (x *ApplePayData) ProtoReflect() protoreflect.Message {
	mi := &file_client_payment_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ApplePayData.ProtoReflect.Descriptor instead.
func (*ApplePayData) Descriptor() ([]byte, []int) {
	return file_client_payment_proto_rawDescGZIP(), []int{4}
}

func (x *ApplePayData) GetJsonData() string {
	if x != nil {
		return x.JsonData
	}
	return ""
}

type ApplePayResult struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code    int32  `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`      // код операции (0- успешная, остальные коды ошибок)
	Message string `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"` // текстовое сообщение по коду
}

func (x *ApplePayResult) Reset() {
	*x = ApplePayResult{}
	if protoimpl.UnsafeEnabled {
		mi := &file_client_payment_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ApplePayResult) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ApplePayResult) ProtoMessage() {}

func (x *ApplePayResult) ProtoReflect() protoreflect.Message {
	mi := &file_client_payment_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ApplePayResult.ProtoReflect.Descriptor instead.
func (*ApplePayResult) Descriptor() ([]byte, []int) {
	return file_client_payment_proto_rawDescGZIP(), []int{5}
}

func (x *ApplePayResult) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *ApplePayResult) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

var File_client_payment_proto protoreflect.FileDescriptor

var file_client_payment_proto_rawDesc = []byte{
	0x0a, 0x14, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x2f, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x15, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65,
	0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x22, 0xe5, 0x01,
	0x0a, 0x06, 0x4d, 0x65, 0x74, 0x68, 0x6f, 0x64, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63,
	0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64,
	0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x18, 0x0a, 0x07, 0x72, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x72, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x04,
	0x20, 0x01, 0x28, 0x01, 0x52, 0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x21, 0x0a, 0x0c,
	0x66, 0x69, 0x6e, 0x61, 0x6c, 0x5f, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x05, 0x20, 0x01,
	0x28, 0x01, 0x52, 0x0b, 0x66, 0x69, 0x6e, 0x61, 0x6c, 0x41, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12,
	0x1a, 0x0a, 0x08, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x18, 0x06, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x12, 0x12, 0x0a, 0x04, 0x74,
	0x79, 0x70, 0x65, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65, 0x12,
	0x12, 0x0a, 0x04, 0x72, 0x61, 0x74, 0x65, 0x18, 0x08, 0x20, 0x01, 0x28, 0x01, 0x52, 0x04, 0x72,
	0x61, 0x74, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x67, 0x65, 0x6f, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x03, 0x67, 0x65, 0x6f, 0x22, 0x84, 0x04, 0x0a, 0x07, 0x49, 0x6e, 0x76, 0x6f, 0x69, 0x63,
	0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69,
	0x64, 0x12, 0x15, 0x0a, 0x06, 0x70, 0x61, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x05, 0x70, 0x61, 0x79, 0x49, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x72, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x72, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x18, 0x04, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x07, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x12, 0x16, 0x0a, 0x06,
	0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x01, 0x52, 0x06, 0x61, 0x6d,
	0x6f, 0x75, 0x6e, 0x74, 0x12, 0x21, 0x0a, 0x0c, 0x66, 0x69, 0x6e, 0x61, 0x6c, 0x5f, 0x61, 0x6d,
	0x6f, 0x75, 0x6e, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x01, 0x52, 0x0b, 0x66, 0x69, 0x6e, 0x61,
	0x6c, 0x41, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x1a, 0x0a, 0x08, 0x63, 0x75, 0x72, 0x72, 0x65,
	0x6e, 0x63, 0x79, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x63, 0x75, 0x72, 0x72, 0x65,
	0x6e, 0x63, 0x79, 0x12, 0x1d, 0x0a, 0x0a, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x75, 0x72,
	0x6c, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x55,
	0x72, 0x6c, 0x12, 0x23, 0x0a, 0x0d, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x6d, 0x65, 0x74,
	0x68, 0x6f, 0x64, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x61, 0x63, 0x74, 0x69, 0x6f,
	0x6e, 0x4d, 0x65, 0x74, 0x68, 0x6f, 0x64, 0x12, 0x4e, 0x0a, 0x0a, 0x70, 0x61, 0x72, 0x61, 0x6d,
	0x65, 0x74, 0x65, 0x72, 0x73, 0x18, 0x0a, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x2e, 0x2e, 0x63, 0x6f,
	0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x63, 0x6c, 0x69,
	0x65, 0x6e, 0x74, 0x2e, 0x49, 0x6e, 0x76, 0x6f, 0x69, 0x63, 0x65, 0x2e, 0x50, 0x61, 0x72, 0x61,
	0x6d, 0x65, 0x74, 0x65, 0x72, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x0a, 0x70, 0x61, 0x72,
	0x61, 0x6d, 0x65, 0x74, 0x65, 0x72, 0x73, 0x12, 0x12, 0x0a, 0x04, 0x72, 0x61, 0x74, 0x65, 0x18,
	0x0b, 0x20, 0x01, 0x28, 0x01, 0x52, 0x04, 0x72, 0x61, 0x74, 0x65, 0x12, 0x1f, 0x0a, 0x0b, 0x73,
	0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x5f, 0x75, 0x72, 0x6c, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0a, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x55, 0x72, 0x6c, 0x12, 0x19, 0x0a, 0x08,
	0x66, 0x61, 0x69, 0x6c, 0x5f, 0x75, 0x72, 0x6c, 0x18, 0x0d, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07,
	0x66, 0x61, 0x69, 0x6c, 0x55, 0x72, 0x6c, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x0e, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x75,
	0x72, 0x6c, 0x18, 0x0f, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x72, 0x6c, 0x1a, 0x3d, 0x0a,
	0x0f, 0x50, 0x61, 0x72, 0x61, 0x6d, 0x65, 0x74, 0x65, 0x72, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79,
	0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6b,
	0x65, 0x79, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x22, 0x56, 0x0a, 0x0a,
	0x53, 0x74, 0x72, 0x69, 0x70, 0x65, 0x44, 0x61, 0x74, 0x61, 0x12, 0x1a, 0x0a, 0x08, 0x63, 0x75,
	0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x63, 0x75,
	0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x12, 0x16, 0x0a, 0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x01, 0x52, 0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x14,
	0x0a, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x74,
	0x6f, 0x6b, 0x65, 0x6e, 0x22, 0x56, 0x0a, 0x0c, 0x53, 0x74, 0x72, 0x69, 0x70, 0x65, 0x52, 0x65,
	0x73, 0x75, 0x6c, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x07, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x12, 0x12,
	0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x63, 0x6f,
	0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x22, 0x2b, 0x0a, 0x0c,
	0x41, 0x70, 0x70, 0x6c, 0x65, 0x50, 0x61, 0x79, 0x44, 0x61, 0x74, 0x61, 0x12, 0x1b, 0x0a, 0x09,
	0x6a, 0x73, 0x6f, 0x6e, 0x5f, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x08, 0x6a, 0x73, 0x6f, 0x6e, 0x44, 0x61, 0x74, 0x61, 0x22, 0x3e, 0x0a, 0x0e, 0x41, 0x70, 0x70,
	0x6c, 0x65, 0x50, 0x61, 0x79, 0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x63,
	0x6f, 0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12,
	0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x32, 0xcf, 0x02, 0x0a, 0x07, 0x50, 0x61,
	0x79, 0x6d, 0x65, 0x6e, 0x74, 0x12, 0x49, 0x0a, 0x07, 0x4d, 0x65, 0x74, 0x68, 0x6f, 0x64, 0x73,
	0x12, 0x1d, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70,
	0x69, 0x2e, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x2e, 0x4d, 0x65, 0x74, 0x68, 0x6f, 0x64, 0x1a,
	0x1d, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69,
	0x2e, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x2e, 0x4d, 0x65, 0x74, 0x68, 0x6f, 0x64, 0x30, 0x01,
	0x12, 0x4f, 0x0a, 0x0d, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x49, 0x6e, 0x76, 0x6f, 0x69, 0x63,
	0x65, 0x12, 0x1e, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61,
	0x70, 0x69, 0x2e, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x2e, 0x49, 0x6e, 0x76, 0x6f, 0x69, 0x63,
	0x65, 0x1a, 0x1e, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61,
	0x70, 0x69, 0x2e, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x2e, 0x49, 0x6e, 0x76, 0x6f, 0x69, 0x63,
	0x65, 0x12, 0x56, 0x0a, 0x08, 0x41, 0x70, 0x70, 0x6c, 0x65, 0x50, 0x61, 0x79, 0x12, 0x23, 0x2e,
	0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x63,
	0x6c, 0x69, 0x65, 0x6e, 0x74, 0x2e, 0x41, 0x70, 0x70, 0x6c, 0x65, 0x50, 0x61, 0x79, 0x44, 0x61,
	0x74, 0x61, 0x1a, 0x25, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e,
	0x61, 0x70, 0x69, 0x2e, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x2e, 0x41, 0x70, 0x70, 0x6c, 0x65,
	0x50, 0x61, 0x79, 0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x12, 0x50, 0x0a, 0x06, 0x53, 0x74, 0x72,
	0x69, 0x70, 0x65, 0x12, 0x21, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65,
	0x2e, 0x61, 0x70, 0x69, 0x2e, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x2e, 0x53, 0x74, 0x72, 0x69,
	0x70, 0x65, 0x44, 0x61, 0x74, 0x61, 0x1a, 0x23, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65,
	0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x2e, 0x53,
	0x74, 0x72, 0x69, 0x70, 0x65, 0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x42, 0x73, 0x0a, 0x15, 0x63,
	0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x63, 0x6c,
	0x69, 0x65, 0x6e, 0x74, 0x42, 0x0b, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x54, 0x79, 0x70,
	0x65, 0x50, 0x00, 0x5a, 0x3d, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f,
	0x42, 0x65, 0x74, 0x61, 0x43, 0x6f, 0x6d, 0x70, 0x61, 0x6e, 0x79, 0x2f, 0x46, 0x72, 0x65, 0x65,
	0x6a, 0x65, 0x2f, 0x73, 0x64, 0x6b, 0x2f, 0x67, 0x6f, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x63, 0x6c,
	0x69, 0x65, 0x6e, 0x74, 0x3b, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x5f, 0x63, 0x6c, 0x69, 0x65,
	0x6e, 0x74, 0xa2, 0x02, 0x0b, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x54, 0x79, 0x70, 0x65,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_client_payment_proto_rawDescOnce sync.Once
	file_client_payment_proto_rawDescData = file_client_payment_proto_rawDesc
)

func file_client_payment_proto_rawDescGZIP() []byte {
	file_client_payment_proto_rawDescOnce.Do(func() {
		file_client_payment_proto_rawDescData = protoimpl.X.CompressGZIP(file_client_payment_proto_rawDescData)
	})
	return file_client_payment_proto_rawDescData
}

var file_client_payment_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_client_payment_proto_goTypes = []interface{}{
	(*Method)(nil),         // 0: com.freeje.api.client.Method
	(*Invoice)(nil),        // 1: com.freeje.api.client.Invoice
	(*StripeData)(nil),     // 2: com.freeje.api.client.StripeData
	(*StripeResult)(nil),   // 3: com.freeje.api.client.StripeResult
	(*ApplePayData)(nil),   // 4: com.freeje.api.client.ApplePayData
	(*ApplePayResult)(nil), // 5: com.freeje.api.client.ApplePayResult
	nil,                    // 6: com.freeje.api.client.Invoice.ParametersEntry
}
var file_client_payment_proto_depIdxs = []int32{
	6, // 0: com.freeje.api.client.Invoice.parameters:type_name -> com.freeje.api.client.Invoice.ParametersEntry
	0, // 1: com.freeje.api.client.Payment.Methods:input_type -> com.freeje.api.client.Method
	1, // 2: com.freeje.api.client.Payment.CreateInvoice:input_type -> com.freeje.api.client.Invoice
	4, // 3: com.freeje.api.client.Payment.ApplePay:input_type -> com.freeje.api.client.ApplePayData
	2, // 4: com.freeje.api.client.Payment.Stripe:input_type -> com.freeje.api.client.StripeData
	0, // 5: com.freeje.api.client.Payment.Methods:output_type -> com.freeje.api.client.Method
	1, // 6: com.freeje.api.client.Payment.CreateInvoice:output_type -> com.freeje.api.client.Invoice
	5, // 7: com.freeje.api.client.Payment.ApplePay:output_type -> com.freeje.api.client.ApplePayResult
	3, // 8: com.freeje.api.client.Payment.Stripe:output_type -> com.freeje.api.client.StripeResult
	5, // [5:9] is the sub-list for method output_type
	1, // [1:5] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_client_payment_proto_init() }
func file_client_payment_proto_init() {
	if File_client_payment_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_client_payment_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Method); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_client_payment_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Invoice); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_client_payment_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StripeData); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_client_payment_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StripeResult); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_client_payment_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ApplePayData); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_client_payment_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ApplePayResult); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_client_payment_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_client_payment_proto_goTypes,
		DependencyIndexes: file_client_payment_proto_depIdxs,
		MessageInfos:      file_client_payment_proto_msgTypes,
	}.Build()
	File_client_payment_proto = out.File
	file_client_payment_proto_rawDesc = nil
	file_client_payment_proto_goTypes = nil
	file_client_payment_proto_depIdxs = nil
}
