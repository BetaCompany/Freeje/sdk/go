// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package freeje_client

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// SMSClient is the client API for SMS service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type SMSClient interface {
	//Отправить СМС
	Send(ctx context.Context, in *SmsMessage, opts ...grpc.CallOption) (*SmsMessage, error)
	//Запросить перечень смс
	Pull(ctx context.Context, in *SmsPull, opts ...grpc.CallOption) (SMS_PullClient, error)
}

type sMSClient struct {
	cc grpc.ClientConnInterface
}

func NewSMSClient(cc grpc.ClientConnInterface) SMSClient {
	return &sMSClient{cc}
}

func (c *sMSClient) Send(ctx context.Context, in *SmsMessage, opts ...grpc.CallOption) (*SmsMessage, error) {
	out := new(SmsMessage)
	err := c.cc.Invoke(ctx, "/com.freeje.api.client.SMS/Send", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *sMSClient) Pull(ctx context.Context, in *SmsPull, opts ...grpc.CallOption) (SMS_PullClient, error) {
	stream, err := c.cc.NewStream(ctx, &SMS_ServiceDesc.Streams[0], "/com.freeje.api.client.SMS/Pull", opts...)
	if err != nil {
		return nil, err
	}
	x := &sMSPullClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type SMS_PullClient interface {
	Recv() (*SmsMessage, error)
	grpc.ClientStream
}

type sMSPullClient struct {
	grpc.ClientStream
}

func (x *sMSPullClient) Recv() (*SmsMessage, error) {
	m := new(SmsMessage)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// SMSServer is the server API for SMS service.
// All implementations should embed UnimplementedSMSServer
// for forward compatibility
type SMSServer interface {
	//Отправить СМС
	Send(context.Context, *SmsMessage) (*SmsMessage, error)
	//Запросить перечень смс
	Pull(*SmsPull, SMS_PullServer) error
}

// UnimplementedSMSServer should be embedded to have forward compatible implementations.
type UnimplementedSMSServer struct {
}

func (UnimplementedSMSServer) Send(context.Context, *SmsMessage) (*SmsMessage, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Send not implemented")
}
func (UnimplementedSMSServer) Pull(*SmsPull, SMS_PullServer) error {
	return status.Errorf(codes.Unimplemented, "method Pull not implemented")
}

// UnsafeSMSServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to SMSServer will
// result in compilation errors.
type UnsafeSMSServer interface {
	mustEmbedUnimplementedSMSServer()
}

func RegisterSMSServer(s grpc.ServiceRegistrar, srv SMSServer) {
	s.RegisterService(&SMS_ServiceDesc, srv)
}

func _SMS_Send_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SmsMessage)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SMSServer).Send(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/com.freeje.api.client.SMS/Send",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SMSServer).Send(ctx, req.(*SmsMessage))
	}
	return interceptor(ctx, in, info, handler)
}

func _SMS_Pull_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(SmsPull)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(SMSServer).Pull(m, &sMSPullServer{stream})
}

type SMS_PullServer interface {
	Send(*SmsMessage) error
	grpc.ServerStream
}

type sMSPullServer struct {
	grpc.ServerStream
}

func (x *sMSPullServer) Send(m *SmsMessage) error {
	return x.ServerStream.SendMsg(m)
}

// SMS_ServiceDesc is the grpc.ServiceDesc for SMS service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var SMS_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "com.freeje.api.client.SMS",
	HandlerType: (*SMSServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Send",
			Handler:    _SMS_Send_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "Pull",
			Handler:       _SMS_Pull_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "client/sms.proto",
}
