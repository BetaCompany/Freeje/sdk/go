// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.18.1
// source: admin/profile.proto

package freeje_admin

import (
	api "gitlab.com/BetaCompany/Freeje/sdk/go/api"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type FreejeProfile struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          string             `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"` // идентификатор аккаунта
	Email       string             `protobuf:"bytes,2,opt,name=email,proto3" json:"email,omitempty"`
	Photo       string             `protobuf:"bytes,3,opt,name=photo,proto3" json:"photo,omitempty"`
	Name        string             `protobuf:"bytes,4,opt,name=name,proto3" json:"name,omitempty"`
	Createdms   int64              `protobuf:"varint,5,opt,name=createdms,proto3" json:"createdms,omitempty"`     // время создания профиля
	Lastusagems int64              `protobuf:"varint,6,opt,name=lastusagems,proto3" json:"lastusagems,omitempty"` // время последнего обращения профилем к API
	Expiredms   int64              `protobuf:"varint,8,opt,name=expiredms,proto3" json:"expiredms,omitempty"`     // время до которого аккаунт считается действиетельным (0 - лимит не установлен)
	Phone       []*api.PhoneNumber `protobuf:"bytes,9,rep,name=phone,proto3" json:"phone,omitempty"`
	Did         []*DidNumber       `protobuf:"bytes,10,rep,name=did,proto3" json:"did,omitempty"`
	Funds       float64            `protobuf:"fixed64,11,opt,name=funds,proto3" json:"funds,omitempty"`
	LockedFunds float64            `protobuf:"fixed64,12,opt,name=lockedFunds,proto3" json:"lockedFunds,omitempty"`
	Meta        *FreejeProfileMeta `protobuf:"bytes,15,opt,name=meta,proto3" json:"meta,omitempty"`
}

func (x *FreejeProfile) Reset() {
	*x = FreejeProfile{}
	if protoimpl.UnsafeEnabled {
		mi := &file_admin_profile_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FreejeProfile) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FreejeProfile) ProtoMessage() {}

func (x *FreejeProfile) ProtoReflect() protoreflect.Message {
	mi := &file_admin_profile_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FreejeProfile.ProtoReflect.Descriptor instead.
func (*FreejeProfile) Descriptor() ([]byte, []int) {
	return file_admin_profile_proto_rawDescGZIP(), []int{0}
}

func (x *FreejeProfile) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *FreejeProfile) GetEmail() string {
	if x != nil {
		return x.Email
	}
	return ""
}

func (x *FreejeProfile) GetPhoto() string {
	if x != nil {
		return x.Photo
	}
	return ""
}

func (x *FreejeProfile) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *FreejeProfile) GetCreatedms() int64 {
	if x != nil {
		return x.Createdms
	}
	return 0
}

func (x *FreejeProfile) GetLastusagems() int64 {
	if x != nil {
		return x.Lastusagems
	}
	return 0
}

func (x *FreejeProfile) GetExpiredms() int64 {
	if x != nil {
		return x.Expiredms
	}
	return 0
}

func (x *FreejeProfile) GetPhone() []*api.PhoneNumber {
	if x != nil {
		return x.Phone
	}
	return nil
}

func (x *FreejeProfile) GetDid() []*DidNumber {
	if x != nil {
		return x.Did
	}
	return nil
}

func (x *FreejeProfile) GetFunds() float64 {
	if x != nil {
		return x.Funds
	}
	return 0
}

func (x *FreejeProfile) GetLockedFunds() float64 {
	if x != nil {
		return x.LockedFunds
	}
	return 0
}

func (x *FreejeProfile) GetMeta() *FreejeProfileMeta {
	if x != nil {
		return x.Meta
	}
	return nil
}

type FreejeProfileMeta struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	TariffId       string                `protobuf:"bytes,1,opt,name=tariffId,proto3" json:"tariffId,omitempty"`                                     // действующий тариф профиля
	TariffName     string                `protobuf:"bytes,2,opt,name=tariffName,proto3" json:"tariffName,omitempty"`                                 // имя действующего тарифа
	Concurrency    int32                 `protobuf:"varint,3,opt,name=concurrency,proto3" json:"concurrency,omitempty"`                              // максимальное количество одновременно задействованных линий (0 - как задано в тарифе)
	Agreement      *api.AccountAgreement `protobuf:"bytes,4,opt,name=agreement,proto3" json:"agreement,omitempty"`                                   // данные по согласию пользователя
	Locked         bool                  `protobuf:"varint,5,opt,name=locked,proto3" json:"locked,omitempty"`                                        // признак блокировки аккаунта
	FirstUserAgent string                `protobuf:"bytes,6,opt,name=first_user_agent,json=firstUserAgent,proto3" json:"first_user_agent,omitempty"` // первый user agent клиента с которого обратился пользователь
}

func (x *FreejeProfileMeta) Reset() {
	*x = FreejeProfileMeta{}
	if protoimpl.UnsafeEnabled {
		mi := &file_admin_profile_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FreejeProfileMeta) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FreejeProfileMeta) ProtoMessage() {}

func (x *FreejeProfileMeta) ProtoReflect() protoreflect.Message {
	mi := &file_admin_profile_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FreejeProfileMeta.ProtoReflect.Descriptor instead.
func (*FreejeProfileMeta) Descriptor() ([]byte, []int) {
	return file_admin_profile_proto_rawDescGZIP(), []int{1}
}

func (x *FreejeProfileMeta) GetTariffId() string {
	if x != nil {
		return x.TariffId
	}
	return ""
}

func (x *FreejeProfileMeta) GetTariffName() string {
	if x != nil {
		return x.TariffName
	}
	return ""
}

func (x *FreejeProfileMeta) GetConcurrency() int32 {
	if x != nil {
		return x.Concurrency
	}
	return 0
}

func (x *FreejeProfileMeta) GetAgreement() *api.AccountAgreement {
	if x != nil {
		return x.Agreement
	}
	return nil
}

func (x *FreejeProfileMeta) GetLocked() bool {
	if x != nil {
		return x.Locked
	}
	return false
}

func (x *FreejeProfileMeta) GetFirstUserAgent() string {
	if x != nil {
		return x.FirstUserAgent
	}
	return ""
}

type Charge struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	UserId string  `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"` // идентификатор пользователя чей баланс меняем
	Delta  float64 `protobuf:"fixed64,2,opt,name=delta,proto3" json:"delta,omitempty"`               // значение на которое следует изменить баланс (положительное - пополнение, отрицательное - списание)
	Notes  string  `protobuf:"bytes,3,opt,name=notes,proto3" json:"notes,omitempty"`                 // комментарий к действию
}

func (x *Charge) Reset() {
	*x = Charge{}
	if protoimpl.UnsafeEnabled {
		mi := &file_admin_profile_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Charge) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Charge) ProtoMessage() {}

func (x *Charge) ProtoReflect() protoreflect.Message {
	mi := &file_admin_profile_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Charge.ProtoReflect.Descriptor instead.
func (*Charge) Descriptor() ([]byte, []int) {
	return file_admin_profile_proto_rawDescGZIP(), []int{2}
}

func (x *Charge) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

func (x *Charge) GetDelta() float64 {
	if x != nil {
		return x.Delta
	}
	return 0
}

func (x *Charge) GetNotes() string {
	if x != nil {
		return x.Notes
	}
	return ""
}

// Баланс счета
type AccountBalance struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Funds       float64 `protobuf:"fixed64,1,opt,name=funds,proto3" json:"funds,omitempty"`
	LockedFunds float64 `protobuf:"fixed64,2,opt,name=lockedFunds,proto3" json:"lockedFunds,omitempty"`
}

func (x *AccountBalance) Reset() {
	*x = AccountBalance{}
	if protoimpl.UnsafeEnabled {
		mi := &file_admin_profile_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AccountBalance) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AccountBalance) ProtoMessage() {}

func (x *AccountBalance) ProtoReflect() protoreflect.Message {
	mi := &file_admin_profile_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AccountBalance.ProtoReflect.Descriptor instead.
func (*AccountBalance) Descriptor() ([]byte, []int) {
	return file_admin_profile_proto_rawDescGZIP(), []int{3}
}

func (x *AccountBalance) GetFunds() float64 {
	if x != nil {
		return x.Funds
	}
	return 0
}

func (x *AccountBalance) GetLockedFunds() float64 {
	if x != nil {
		return x.LockedFunds
	}
	return 0
}

type Keyspace struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name    string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`       // идентификатор используемого keyspace в базе данных
	Version string `protobuf:"bytes,2,opt,name=version,proto3" json:"version,omitempty"` // версия бэка
}

func (x *Keyspace) Reset() {
	*x = Keyspace{}
	if protoimpl.UnsafeEnabled {
		mi := &file_admin_profile_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Keyspace) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Keyspace) ProtoMessage() {}

func (x *Keyspace) ProtoReflect() protoreflect.Message {
	mi := &file_admin_profile_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Keyspace.ProtoReflect.Descriptor instead.
func (*Keyspace) Descriptor() ([]byte, []int) {
	return file_admin_profile_proto_rawDescGZIP(), []int{4}
}

func (x *Keyspace) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *Keyspace) GetVersion() string {
	if x != nil {
		return x.Version
	}
	return ""
}

var File_admin_profile_proto protoreflect.FileDescriptor

var file_admin_profile_proto_rawDesc = []byte{
	0x0a, 0x13, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2f, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x14, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a,
	0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x1a, 0x0c, 0x63, 0x6f, 0x6d,
	0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x0f, 0x61, 0x64, 0x6d, 0x69, 0x6e,
	0x2f, 0x64, 0x69, 0x64, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x94, 0x03, 0x0a, 0x0d, 0x46,
	0x72, 0x65, 0x65, 0x6a, 0x65, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05,
	0x65, 0x6d, 0x61, 0x69, 0x6c, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x6d, 0x61,
	0x69, 0x6c, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1c, 0x0a, 0x09,
	0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x6d, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x6d, 0x73, 0x12, 0x20, 0x0a, 0x0b, 0x6c, 0x61,
	0x73, 0x74, 0x75, 0x73, 0x61, 0x67, 0x65, 0x6d, 0x73, 0x18, 0x06, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x0b, 0x6c, 0x61, 0x73, 0x74, 0x75, 0x73, 0x61, 0x67, 0x65, 0x6d, 0x73, 0x12, 0x1c, 0x0a, 0x09,
	0x65, 0x78, 0x70, 0x69, 0x72, 0x65, 0x64, 0x6d, 0x73, 0x18, 0x08, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x09, 0x65, 0x78, 0x70, 0x69, 0x72, 0x65, 0x64, 0x6d, 0x73, 0x12, 0x2d, 0x0a, 0x05, 0x70, 0x68,
	0x6f, 0x6e, 0x65, 0x18, 0x09, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x63, 0x6f, 0x6d, 0x2e,
	0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x50, 0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62,
	0x65, 0x72, 0x52, 0x05, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x12, 0x31, 0x0a, 0x03, 0x64, 0x69, 0x64,
	0x18, 0x0a, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1f, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65,
	0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x44, 0x69,
	0x64, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x52, 0x03, 0x64, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05,
	0x66, 0x75, 0x6e, 0x64, 0x73, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x01, 0x52, 0x05, 0x66, 0x75, 0x6e,
	0x64, 0x73, 0x12, 0x20, 0x0a, 0x0b, 0x6c, 0x6f, 0x63, 0x6b, 0x65, 0x64, 0x46, 0x75, 0x6e, 0x64,
	0x73, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x01, 0x52, 0x0b, 0x6c, 0x6f, 0x63, 0x6b, 0x65, 0x64, 0x46,
	0x75, 0x6e, 0x64, 0x73, 0x12, 0x3b, 0x0a, 0x04, 0x6d, 0x65, 0x74, 0x61, 0x18, 0x0f, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x27, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e,
	0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x46, 0x72, 0x65, 0x65, 0x6a, 0x65,
	0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x4d, 0x65, 0x74, 0x61, 0x52, 0x04, 0x6d, 0x65, 0x74,
	0x61, 0x22, 0xef, 0x01, 0x0a, 0x11, 0x46, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x50, 0x72, 0x6f, 0x66,
	0x69, 0x6c, 0x65, 0x4d, 0x65, 0x74, 0x61, 0x12, 0x1a, 0x0a, 0x08, 0x74, 0x61, 0x72, 0x69, 0x66,
	0x66, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x74, 0x61, 0x72, 0x69, 0x66,
	0x66, 0x49, 0x64, 0x12, 0x1e, 0x0a, 0x0a, 0x74, 0x61, 0x72, 0x69, 0x66, 0x66, 0x4e, 0x61, 0x6d,
	0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x74, 0x61, 0x72, 0x69, 0x66, 0x66, 0x4e,
	0x61, 0x6d, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x63, 0x6f, 0x6e, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e,
	0x63, 0x79, 0x18, 0x03, 0x20, 0x01, 0x28, 0x05, 0x52, 0x0b, 0x63, 0x6f, 0x6e, 0x63, 0x75, 0x72,
	0x72, 0x65, 0x6e, 0x63, 0x79, 0x12, 0x3a, 0x0a, 0x09, 0x61, 0x67, 0x72, 0x65, 0x65, 0x6d, 0x65,
	0x6e, 0x74, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66,
	0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x41, 0x63, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x41, 0x67, 0x72,
	0x65, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x52, 0x09, 0x61, 0x67, 0x72, 0x65, 0x65, 0x6d, 0x65, 0x6e,
	0x74, 0x12, 0x16, 0x0a, 0x06, 0x6c, 0x6f, 0x63, 0x6b, 0x65, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x08, 0x52, 0x06, 0x6c, 0x6f, 0x63, 0x6b, 0x65, 0x64, 0x12, 0x28, 0x0a, 0x10, 0x66, 0x69, 0x72,
	0x73, 0x74, 0x5f, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x61, 0x67, 0x65, 0x6e, 0x74, 0x18, 0x06, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0e, 0x66, 0x69, 0x72, 0x73, 0x74, 0x55, 0x73, 0x65, 0x72, 0x41, 0x67,
	0x65, 0x6e, 0x74, 0x22, 0x4d, 0x0a, 0x06, 0x43, 0x68, 0x61, 0x72, 0x67, 0x65, 0x12, 0x17, 0x0a,
	0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06,
	0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x64, 0x65, 0x6c, 0x74, 0x61, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x01, 0x52, 0x05, 0x64, 0x65, 0x6c, 0x74, 0x61, 0x12, 0x14, 0x0a, 0x05,
	0x6e, 0x6f, 0x74, 0x65, 0x73, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x6e, 0x6f, 0x74,
	0x65, 0x73, 0x22, 0x48, 0x0a, 0x0e, 0x41, 0x63, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x42, 0x61, 0x6c,
	0x61, 0x6e, 0x63, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x66, 0x75, 0x6e, 0x64, 0x73, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x01, 0x52, 0x05, 0x66, 0x75, 0x6e, 0x64, 0x73, 0x12, 0x20, 0x0a, 0x0b, 0x6c, 0x6f,
	0x63, 0x6b, 0x65, 0x64, 0x46, 0x75, 0x6e, 0x64, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x01, 0x52,
	0x0b, 0x6c, 0x6f, 0x63, 0x6b, 0x65, 0x64, 0x46, 0x75, 0x6e, 0x64, 0x73, 0x22, 0x38, 0x0a, 0x08,
	0x4b, 0x65, 0x79, 0x73, 0x70, 0x61, 0x63, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x18, 0x0a, 0x07,
	0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x76,
	0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x32, 0xea, 0x06, 0x0a, 0x07, 0x50, 0x72, 0x6f, 0x66, 0x69,
	0x6c, 0x65, 0x12, 0x39, 0x0a, 0x04, 0x41, 0x75, 0x74, 0x68, 0x12, 0x11, 0x2e, 0x63, 0x6f, 0x6d,
	0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a, 0x1e, 0x2e,
	0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61,
	0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x4b, 0x65, 0x79, 0x73, 0x70, 0x61, 0x63, 0x65, 0x12, 0x4f, 0x0a,
	0x0c, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x73, 0x12, 0x18, 0x2e,
	0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x53, 0x65, 0x61, 0x72, 0x63,
	0x68, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x1a, 0x23, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72,
	0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x46,
	0x72, 0x65, 0x65, 0x6a, 0x65, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x30, 0x01, 0x12, 0x5c,
	0x0a, 0x0e, 0x4d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65,
	0x12, 0x23, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70,
	0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x46, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x50, 0x72,
	0x6f, 0x66, 0x69, 0x6c, 0x65, 0x1a, 0x23, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65,
	0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x46, 0x72, 0x65,
	0x65, 0x6a, 0x65, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x30, 0x01, 0x12, 0x59, 0x0a, 0x0d,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x23, 0x2e,
	0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61,
	0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x46, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x50, 0x72, 0x6f, 0x66, 0x69,
	0x6c, 0x65, 0x1a, 0x23, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e,
	0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x46, 0x72, 0x65, 0x65, 0x6a, 0x65,
	0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x53, 0x0a, 0x0d, 0x43, 0x68, 0x61, 0x72, 0x67,
	0x65, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66,
	0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e,
	0x43, 0x68, 0x61, 0x72, 0x67, 0x65, 0x1a, 0x24, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65,
	0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x41, 0x63,
	0x63, 0x6f, 0x75, 0x6e, 0x74, 0x42, 0x61, 0x6c, 0x61, 0x6e, 0x63, 0x65, 0x12, 0x54, 0x0a, 0x16,
	0x55, 0x6e, 0x6c, 0x6f, 0x63, 0x6b, 0x46, 0x69, 0x6e, 0x61, 0x6e, 0x63, 0x65, 0x4f, 0x70, 0x65,
	0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65,
	0x65, 0x6a, 0x65, 0x2e, 0x46, 0x69, 0x6e, 0x61, 0x6e, 0x63, 0x65, 0x4f, 0x70, 0x65, 0x72, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x1a, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a,
	0x65, 0x2e, 0x46, 0x69, 0x6e, 0x61, 0x6e, 0x63, 0x65, 0x4f, 0x70, 0x65, 0x72, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x12, 0x57, 0x0a, 0x0f, 0x47, 0x65, 0x74, 0x44, 0x6f, 0x63, 0x75, 0x6d, 0x65, 0x6e,
	0x74, 0x44, 0x61, 0x74, 0x61, 0x12, 0x21, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65,
	0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x44, 0x69, 0x64,
	0x44, 0x6f, 0x63, 0x75, 0x6d, 0x65, 0x6e, 0x74, 0x1a, 0x21, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66,
	0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e,
	0x44, 0x69, 0x64, 0x44, 0x6f, 0x63, 0x75, 0x6d, 0x65, 0x6e, 0x74, 0x12, 0x5f, 0x0a, 0x13, 0x47,
	0x65, 0x74, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x44, 0x6f, 0x63, 0x75, 0x6d, 0x65, 0x6e,
	0x74, 0x73, 0x12, 0x23, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e,
	0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x46, 0x72, 0x65, 0x65, 0x6a, 0x65,
	0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x1a, 0x21, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72,
	0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x44,
	0x69, 0x64, 0x44, 0x6f, 0x63, 0x75, 0x6d, 0x65, 0x6e, 0x74, 0x30, 0x01, 0x12, 0x5d, 0x0a, 0x11,
	0x4c, 0x69, 0x6e, 0x6b, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x50, 0x68, 0x6f, 0x6e, 0x65,
	0x73, 0x12, 0x23, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61,
	0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x46, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x50,
	0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x1a, 0x23, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65,
	0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x2e, 0x46, 0x72,
	0x65, 0x65, 0x6a, 0x65, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x56, 0x0a, 0x12, 0x42,
	0x75, 0x69, 0x6c, 0x64, 0x46, 0x69, 0x6e, 0x61, 0x6e, 0x63, 0x65, 0x52, 0x65, 0x70, 0x6f, 0x72,
	0x74, 0x12, 0x20, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x52,
	0x65, 0x70, 0x6f, 0x72, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x4f, 0x70, 0x74, 0x69,
	0x6f, 0x6e, 0x73, 0x1a, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65,
	0x2e, 0x46, 0x69, 0x6e, 0x61, 0x6e, 0x63, 0x65, 0x4f, 0x70, 0x65, 0x72, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x30, 0x01, 0x42, 0x70, 0x0a, 0x14, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a,
	0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x42, 0x0b, 0x50, 0x72, 0x6f,
	0x66, 0x69, 0x6c, 0x65, 0x54, 0x79, 0x70, 0x65, 0x50, 0x00, 0x5a, 0x3b, 0x67, 0x69, 0x74, 0x6c,
	0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x42, 0x65, 0x74, 0x61, 0x43, 0x6f, 0x6d, 0x70, 0x61,
	0x6e, 0x79, 0x2f, 0x46, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2f, 0x73, 0x64, 0x6b, 0x2f, 0x67, 0x6f,
	0x2f, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x3b, 0x66, 0x72, 0x65, 0x65, 0x6a,
	0x65, 0x5f, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0xa2, 0x02, 0x0b, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c,
	0x65, 0x54, 0x79, 0x70, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_admin_profile_proto_rawDescOnce sync.Once
	file_admin_profile_proto_rawDescData = file_admin_profile_proto_rawDesc
)

func file_admin_profile_proto_rawDescGZIP() []byte {
	file_admin_profile_proto_rawDescOnce.Do(func() {
		file_admin_profile_proto_rawDescData = protoimpl.X.CompressGZIP(file_admin_profile_proto_rawDescData)
	})
	return file_admin_profile_proto_rawDescData
}

var file_admin_profile_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_admin_profile_proto_goTypes = []interface{}{
	(*FreejeProfile)(nil),            // 0: com.freeje.api.admin.FreejeProfile
	(*FreejeProfileMeta)(nil),        // 1: com.freeje.api.admin.FreejeProfileMeta
	(*Charge)(nil),                   // 2: com.freeje.api.admin.Charge
	(*AccountBalance)(nil),           // 3: com.freeje.api.admin.AccountBalance
	(*Keyspace)(nil),                 // 4: com.freeje.api.admin.Keyspace
	(*api.PhoneNumber)(nil),          // 5: com.freeje.PhoneNumber
	(*DidNumber)(nil),                // 6: com.freeje.api.admin.DidNumber
	(*api.AccountAgreement)(nil),     // 7: com.freeje.AccountAgreement
	(*api.Empty)(nil),                // 8: com.freeje.Empty
	(*api.SearchFilter)(nil),         // 9: com.freeje.SearchFilter
	(*api.FinanceOperation)(nil),     // 10: com.freeje.FinanceOperation
	(*DidDocument)(nil),              // 11: com.freeje.api.admin.DidDocument
	(*api.ReportRequestOptions)(nil), // 12: com.freeje.ReportRequestOptions
}
var file_admin_profile_proto_depIdxs = []int32{
	5,  // 0: com.freeje.api.admin.FreejeProfile.phone:type_name -> com.freeje.PhoneNumber
	6,  // 1: com.freeje.api.admin.FreejeProfile.did:type_name -> com.freeje.api.admin.DidNumber
	1,  // 2: com.freeje.api.admin.FreejeProfile.meta:type_name -> com.freeje.api.admin.FreejeProfileMeta
	7,  // 3: com.freeje.api.admin.FreejeProfileMeta.agreement:type_name -> com.freeje.AccountAgreement
	8,  // 4: com.freeje.api.admin.Profile.Auth:input_type -> com.freeje.Empty
	9,  // 5: com.freeje.api.admin.Profile.ListProfiles:input_type -> com.freeje.SearchFilter
	0,  // 6: com.freeje.api.admin.Profile.MonitorProfile:input_type -> com.freeje.api.admin.FreejeProfile
	0,  // 7: com.freeje.api.admin.Profile.UpdateProfile:input_type -> com.freeje.api.admin.FreejeProfile
	2,  // 8: com.freeje.api.admin.Profile.ChargeProfile:input_type -> com.freeje.api.admin.Charge
	10, // 9: com.freeje.api.admin.Profile.UnlockFinanceOperation:input_type -> com.freeje.FinanceOperation
	11, // 10: com.freeje.api.admin.Profile.GetDocumentData:input_type -> com.freeje.api.admin.DidDocument
	0,  // 11: com.freeje.api.admin.Profile.GetProfileDocuments:input_type -> com.freeje.api.admin.FreejeProfile
	0,  // 12: com.freeje.api.admin.Profile.LinkProfilePhones:input_type -> com.freeje.api.admin.FreejeProfile
	12, // 13: com.freeje.api.admin.Profile.BuildFinanceReport:input_type -> com.freeje.ReportRequestOptions
	4,  // 14: com.freeje.api.admin.Profile.Auth:output_type -> com.freeje.api.admin.Keyspace
	0,  // 15: com.freeje.api.admin.Profile.ListProfiles:output_type -> com.freeje.api.admin.FreejeProfile
	0,  // 16: com.freeje.api.admin.Profile.MonitorProfile:output_type -> com.freeje.api.admin.FreejeProfile
	0,  // 17: com.freeje.api.admin.Profile.UpdateProfile:output_type -> com.freeje.api.admin.FreejeProfile
	3,  // 18: com.freeje.api.admin.Profile.ChargeProfile:output_type -> com.freeje.api.admin.AccountBalance
	10, // 19: com.freeje.api.admin.Profile.UnlockFinanceOperation:output_type -> com.freeje.FinanceOperation
	11, // 20: com.freeje.api.admin.Profile.GetDocumentData:output_type -> com.freeje.api.admin.DidDocument
	11, // 21: com.freeje.api.admin.Profile.GetProfileDocuments:output_type -> com.freeje.api.admin.DidDocument
	0,  // 22: com.freeje.api.admin.Profile.LinkProfilePhones:output_type -> com.freeje.api.admin.FreejeProfile
	10, // 23: com.freeje.api.admin.Profile.BuildFinanceReport:output_type -> com.freeje.FinanceOperation
	14, // [14:24] is the sub-list for method output_type
	4,  // [4:14] is the sub-list for method input_type
	4,  // [4:4] is the sub-list for extension type_name
	4,  // [4:4] is the sub-list for extension extendee
	0,  // [0:4] is the sub-list for field type_name
}

func init() { file_admin_profile_proto_init() }
func file_admin_profile_proto_init() {
	if File_admin_profile_proto != nil {
		return
	}
	file_admin_did_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_admin_profile_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FreejeProfile); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_admin_profile_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FreejeProfileMeta); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_admin_profile_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Charge); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_admin_profile_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AccountBalance); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_admin_profile_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Keyspace); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_admin_profile_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_admin_profile_proto_goTypes,
		DependencyIndexes: file_admin_profile_proto_depIdxs,
		MessageInfos:      file_admin_profile_proto_msgTypes,
	}.Build()
	File_admin_profile_proto = out.File
	file_admin_profile_proto_rawDesc = nil
	file_admin_profile_proto_goTypes = nil
	file_admin_profile_proto_depIdxs = nil
}
