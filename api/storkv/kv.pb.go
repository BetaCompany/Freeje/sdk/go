// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.18.1
// source: stor/kv.proto

package storkv

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Pair struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Namespace string `protobuf:"bytes,1,opt,name=namespace,proto3" json:"namespace,omitempty"`
	Key       string `protobuf:"bytes,2,opt,name=key,proto3" json:"key,omitempty"`
	Value     string `protobuf:"bytes,3,opt,name=value,proto3" json:"value,omitempty"`
	Ttl       int32  `protobuf:"varint,4,opt,name=ttl,proto3" json:"ttl,omitempty"` // время жизни пары в секундах
}

func (x *Pair) Reset() {
	*x = Pair{}
	if protoimpl.UnsafeEnabled {
		mi := &file_stor_kv_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Pair) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Pair) ProtoMessage() {}

func (x *Pair) ProtoReflect() protoreflect.Message {
	mi := &file_stor_kv_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Pair.ProtoReflect.Descriptor instead.
func (*Pair) Descriptor() ([]byte, []int) {
	return file_stor_kv_proto_rawDescGZIP(), []int{0}
}

func (x *Pair) GetNamespace() string {
	if x != nil {
		return x.Namespace
	}
	return ""
}

func (x *Pair) GetKey() string {
	if x != nil {
		return x.Key
	}
	return ""
}

func (x *Pair) GetValue() string {
	if x != nil {
		return x.Value
	}
	return ""
}

func (x *Pair) GetTtl() int32 {
	if x != nil {
		return x.Ttl
	}
	return 0
}

var File_stor_kv_proto protoreflect.FileDescriptor

var file_stor_kv_proto_rawDesc = []byte{
	0x0a, 0x0d, 0x73, 0x74, 0x6f, 0x72, 0x2f, 0x6b, 0x76, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x16, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e,
	0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x22, 0x5e, 0x0a, 0x04, 0x50, 0x61, 0x69, 0x72, 0x12,
	0x1c, 0x0a, 0x09, 0x6e, 0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x09, 0x6e, 0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65, 0x12, 0x10, 0x0a,
	0x03, 0x6b, 0x65, 0x79, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12,
	0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x74, 0x74, 0x6c, 0x18, 0x04, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x03, 0x74, 0x74, 0x6c, 0x32, 0xa6, 0x03, 0x0a, 0x02, 0x4b, 0x56, 0x12, 0x41,
	0x0a, 0x03, 0x47, 0x65, 0x74, 0x12, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65,
	0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x2e, 0x50,
	0x61, 0x69, 0x72, 0x1a, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65,
	0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x2e, 0x50, 0x61, 0x69,
	0x72, 0x12, 0x41, 0x0a, 0x03, 0x50, 0x75, 0x74, 0x12, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66,
	0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b,
	0x76, 0x2e, 0x50, 0x61, 0x69, 0x72, 0x1a, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65,
	0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x2e,
	0x50, 0x61, 0x69, 0x72, 0x12, 0x44, 0x0a, 0x06, 0x53, 0x65, 0x74, 0x41, 0x64, 0x64, 0x12, 0x1c,
	0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e,
	0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x2e, 0x50, 0x61, 0x69, 0x72, 0x1a, 0x1c, 0x2e, 0x63,
	0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73, 0x74,
	0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x2e, 0x50, 0x61, 0x69, 0x72, 0x12, 0x44, 0x0a, 0x06, 0x53, 0x65,
	0x74, 0x44, 0x65, 0x6c, 0x12, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a,
	0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x2e, 0x50, 0x61,
	0x69, 0x72, 0x1a, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e,
	0x61, 0x70, 0x69, 0x2e, 0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x2e, 0x50, 0x61, 0x69, 0x72,
	0x12, 0x46, 0x0a, 0x08, 0x53, 0x65, 0x74, 0x43, 0x6c, 0x65, 0x61, 0x72, 0x12, 0x1c, 0x2e, 0x63,
	0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73, 0x74,
	0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x2e, 0x50, 0x61, 0x69, 0x72, 0x1a, 0x1c, 0x2e, 0x63, 0x6f, 0x6d,
	0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73, 0x74, 0x6f, 0x72,
	0x2e, 0x6b, 0x76, 0x2e, 0x50, 0x61, 0x69, 0x72, 0x12, 0x46, 0x0a, 0x06, 0x53, 0x65, 0x74, 0x47,
	0x65, 0x74, 0x12, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e,
	0x61, 0x70, 0x69, 0x2e, 0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x2e, 0x50, 0x61, 0x69, 0x72,
	0x1a, 0x1c, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61, 0x70,
	0x69, 0x2e, 0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x2e, 0x50, 0x61, 0x69, 0x72, 0x30, 0x01,
	0x42, 0x63, 0x0a, 0x16, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x72, 0x65, 0x65, 0x6a, 0x65, 0x2e, 0x61,
	0x70, 0x69, 0x2e, 0x73, 0x74, 0x6f, 0x72, 0x2e, 0x6b, 0x76, 0x42, 0x06, 0x4b, 0x56, 0x54, 0x79,
	0x70, 0x65, 0x50, 0x00, 0x5a, 0x36, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d,
	0x2f, 0x42, 0x65, 0x74, 0x61, 0x43, 0x6f, 0x6d, 0x70, 0x61, 0x6e, 0x79, 0x2f, 0x46, 0x72, 0x65,
	0x65, 0x6a, 0x65, 0x2f, 0x73, 0x64, 0x6b, 0x2f, 0x67, 0x6f, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x73,
	0x74, 0x6f, 0x72, 0x6b, 0x76, 0x3b, 0x73, 0x74, 0x6f, 0x72, 0x6b, 0x76, 0xa2, 0x02, 0x06, 0x4b,
	0x56, 0x54, 0x79, 0x70, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_stor_kv_proto_rawDescOnce sync.Once
	file_stor_kv_proto_rawDescData = file_stor_kv_proto_rawDesc
)

func file_stor_kv_proto_rawDescGZIP() []byte {
	file_stor_kv_proto_rawDescOnce.Do(func() {
		file_stor_kv_proto_rawDescData = protoimpl.X.CompressGZIP(file_stor_kv_proto_rawDescData)
	})
	return file_stor_kv_proto_rawDescData
}

var file_stor_kv_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_stor_kv_proto_goTypes = []interface{}{
	(*Pair)(nil), // 0: com.freeje.api.stor.kv.Pair
}
var file_stor_kv_proto_depIdxs = []int32{
	0, // 0: com.freeje.api.stor.kv.KV.Get:input_type -> com.freeje.api.stor.kv.Pair
	0, // 1: com.freeje.api.stor.kv.KV.Put:input_type -> com.freeje.api.stor.kv.Pair
	0, // 2: com.freeje.api.stor.kv.KV.SetAdd:input_type -> com.freeje.api.stor.kv.Pair
	0, // 3: com.freeje.api.stor.kv.KV.SetDel:input_type -> com.freeje.api.stor.kv.Pair
	0, // 4: com.freeje.api.stor.kv.KV.SetClear:input_type -> com.freeje.api.stor.kv.Pair
	0, // 5: com.freeje.api.stor.kv.KV.SetGet:input_type -> com.freeje.api.stor.kv.Pair
	0, // 6: com.freeje.api.stor.kv.KV.Get:output_type -> com.freeje.api.stor.kv.Pair
	0, // 7: com.freeje.api.stor.kv.KV.Put:output_type -> com.freeje.api.stor.kv.Pair
	0, // 8: com.freeje.api.stor.kv.KV.SetAdd:output_type -> com.freeje.api.stor.kv.Pair
	0, // 9: com.freeje.api.stor.kv.KV.SetDel:output_type -> com.freeje.api.stor.kv.Pair
	0, // 10: com.freeje.api.stor.kv.KV.SetClear:output_type -> com.freeje.api.stor.kv.Pair
	0, // 11: com.freeje.api.stor.kv.KV.SetGet:output_type -> com.freeje.api.stor.kv.Pair
	6, // [6:12] is the sub-list for method output_type
	0, // [0:6] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_stor_kv_proto_init() }
func file_stor_kv_proto_init() {
	if File_stor_kv_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_stor_kv_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Pair); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_stor_kv_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_stor_kv_proto_goTypes,
		DependencyIndexes: file_stor_kv_proto_depIdxs,
		MessageInfos:      file_stor_kv_proto_msgTypes,
	}.Build()
	File_stor_kv_proto = out.File
	file_stor_kv_proto_rawDesc = nil
	file_stor_kv_proto_goTypes = nil
	file_stor_kv_proto_depIdxs = nil
}
