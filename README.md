# Freeje GO simple client examples

Follow these simple rules to start using Freeje API.

### Prerequisites

[Install GO](https://golang.org/doc/install).

### Installing examples

```
go install gitlab.com/BetaCompany/Freeje/sdk/go/...@latest
```

The examples should be installed under go binaries path. Run then one by one.

```
$ dial_price --help 


$ available_numbers --help 
```


Or clone repository to your working folder

```
git clone https://gitlab.com/BetaCompany/Freeje/sdk/go.git freejesdk
cd freejesdk
```

Then inspect and run examples one by one 

```
go run examples/available_numbers/main.go
```

Finally you should see listing of available phones on specified prefix.

```
go run examples/dial_price/main.go
```


Inspect the [original proto](https://gitlab.com/BetaCompany/Freeje/sdk/proto) files as the documentation source for an API.
